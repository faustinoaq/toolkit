#!/usr/bin/env bash

# Script for run/stop charge alarm

while true
do
  cat /sys/class/power_supply/BAT0/status | grep -i Discharging
  if [[ $? -eq 0 ]]
  then
    if [[ -f charger.lock ]]
    then
      pkill vlc
      exit 1
    else
      amixer -q sset Master unmute
      amixer -q sset Master 100%
      touch charger.lock
      cvlc --loop alarm.oga
      rm charger.lock
      exit 0
    fi
  fi
  sleep 1
  printf " Charging... $(date)\r"
done

