#!/bin/bash

# Script for run/stop alarm
if [[ -f alarm.lock ]]
then
  pkill vlc
  exit 1
else
  amixer -q sset Master unmute
  amixer -q sset Master 100%
  touch alarm.lock
  cvlc --loop alarm.oga
  echo "Removing lock..."
  rm alarm.lock
  exit 0
fi
