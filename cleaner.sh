#!/bin/bash

# Script for clean my home directory

# Main user directory
MAIN=/home/main

function del {
  DIR=$MAIN/$1
  if [[ -f $DIR || -d $DIR ]]
  then
    err=$(rm -rf $DIR)
    if [[ $err -eq 0 ]]
    then
      echo "$1 - Done!"
    else
      echo "$1 - Error $err"
      exit 1
    fi
  fi
}

echo "Cleaning home directory..."
echo

del .netbeans/8.2/var/filehistory
del .netbeand/8.2/config/Preferences/org/netbeans/modules/utilities/RecentFilesHistory.properties

del .local/share/recently-used.xbel

del .gradle

del .cache/crystal

del .bash_history

del .node_repl_history

del .cache/thumbnails

del .recently-used

del .python_history

del .lesshst

del .config/Code/Cache
del .config/Code/User/workspaceStorage
del .config/Code/Backups/workspaces.json
del Microsoft

del .adobe

del .fltk

del .mplayer

del .ssr

del .xine

del .local/share/Trash/files
del .local/share/Trash/info
del .local/share/Trash/directorysizes

del .local/share/baloo/index

echo
echo "TODO (manually)"
echo "1. Clean caches:"
echo "- Chromium"
echo "- Firefox"
echo "- GIMP"
echo "- Kate"
echo "- Octopi"
echo "- Okular"
echo "- LibreOffice"
echo "- Plama panel"
echo "- VLC"
echo "2. Check Updates:"
echo "- Android Studio"
echo "- Netbeans"
echo "- Octopi"
echo "- Nodejs"
echo
echo "Completed without errors!"
exit 0
